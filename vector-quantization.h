#ifndef VECTOR_QUANTIZATION_H
#define VECTOR_QUANTIZATION_H

#include <stdio.h>
#include <math.h>

typedef struct Point Point;

struct Point {
	double output;
	double inputs[];
};

void update_inputs(Point *p, Point *q, int dim, double gamma);
void update_output(Point *p, Point *q, double alpha);

double distance(Point *p, Point *q, int dim);

#endif /* VECTOR_QUANTIZATION_H */
