#include "vector-quantization.h"

double distance(Point *p, Point *q, int dim) {
	double sqrtd, d;

	for(int i = 0; i < dim; i++) {
		printf("%lf - %lf\n", p->inputs[i], q->inputs[i]);
		sqrtd = p->inputs[i] - q->inputs[i];
		d += sqrtd * sqrtd;
	}

	return sqrt(d);
}

void update_inputs(Point *p, Point *q, int dim, double gamma) {
	for(int i = 0; i < dim; i++) {
		p->inputs[i] = p->inputs[i] + (gamma * (q->inputs[i] - p->inputs[i]));
	}
}

void update_output(Point *p, Point *q, double alpha) {
	p->output = p->output + (alpha * (q->output - p->output));
}

int main(int argc, char *args[]) {
	int nClusters, nPoints, dim;

	double alpha, gamma;

	printf("Currently, this program only supports fixed learning rates.\n");

	printf("Enter the number of clusters:\n");
	scanf("%d", &nClusters);

	printf("Enter the number of data points:\n");
	scanf("%d", &nPoints);

	printf("A single output field y is assumed.\nEnter the number of inputs (coordinates) per data point:\n");
	scanf("%d", &dim);

	printf("Enter the unsupervised learning rate for the cluster core input values (0 < n < 1):\n");
	scanf("%lf", &gamma);

	printf("Enter the supervised learning rate for the cluster core output value (0 < n < 1):\n");
	scanf("%lf", &alpha);

	Point clusters[nClusters];
	Point points[nPoints];

	printf("All weight vectors will have their output value set to 0 initially.\n\n");

	for(int i = 0; i < nClusters; i++) {
		printf("Cluster %d.\n", i + 1);

		for(int j = 0; j < dim; j++) {
			printf("\tEnter the initial value of input x%d for this cluster's weight vector:\n\t", j + 1);
			scanf("%lf", &clusters[i].inputs[j]);
		}

		clusters[i].output = 0;
	}

	for(int i = 0; i < nPoints; i++) {
		int k = 0;
		double d, min;

		printf("Data point %d.\n", i + 1);

		for(int j = 0; j < dim; j++) {
			printf("\tInput x%d:\n\t", j + 1);
			scanf("%lf", &points[i].inputs[j]);
		}

		printf("\tOutput y:\n\t");
		scanf("%lf", &points[i].output);

		for(int j = 0; j < nClusters; j++) {
			d = distance(&clusters[j], &points[i], dim);

			if(j == 0 || d < min) {
				min = d;
				k = j;
			}
		}

		printf("The weight vector of cluster %d is closest to this data point.\n", k + 1);

		update_inputs(&clusters[k], &points[i], dim, gamma);
		update_output(&clusters[k], &points[i], alpha);

		printf("Calculated new weight vector for cluster %d: (", k + 1);

		for(int j = 0; j < dim; j++) {
			printf("x%d: %lf, ", j, clusters[i].inputs[j]);
		}

		printf("y: %lf).\n", clusters[i].output);
	}

	return 0;
}
