export CFLAGS=-Wall -g -lm

SRC := $(wildcard *.c)
DEST := $(SRC:%.c=%)

all:	$(DEST)

test:
	@echo "SRC=$(SRC)"
	@echo "DEST=$(DEST)"

clean:
	rm -f $(DEST)
